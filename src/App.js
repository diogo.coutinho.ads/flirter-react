import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';

import firebase from 'firebase';

import Router from './Router';

const customTextProps = {
  style: {
    fontFamily: 'roboto-regular'
  }
};

console.disableYellowBox = true;

class App extends Component {
  componentWillMount () {
    //Posso Fazer qualquer tipo de configuração global aqui como por exemplo o Firebase
    if (firebase.apps.length === 0) {
      firebase.initializeApp({
        apiKey: "AIzaSyCRbljQHS5ChzBDY0IpqBZJ2qc713qpOds",
        authDomain: "teste-app-28b66.firebaseio.com",
        databaseURL: "https://teste-app-28b66.firebaseio.com",
        projectId: "teste-app-28b66",
        storageBucket: "teste-app-28b66.firebaseio.com",
        messagingSenderId: "454998636071"
      })
    }
  }

  render() {
    return (
      <Router></Router>
    );
  }
}

export default App